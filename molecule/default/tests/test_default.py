import os
import testinfra.utils.ansible_runner
import pwd

pkg_name='haproxy'
svc_name='haproxy'
haproxy_user='haproxy'
haproxy_group='haproxy'
haproxy_folder='/var/lib/haproxy'

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")

def test_nginx_directory_exists(host):
    haproxy_dir_exists = host.file("/var/lib/haproxy").exists
    assert haproxy_dir_exists

def haproxy_is_installed(host):
    haproxy = host.package(pkg_name)
    assert haproxy.is_installed

def haproxy_running_and_enabled(host):
    haproxy = host.service(svc_name)
    assert haproxy.is_running
    assert haproxy.is_enabled
    
def test_haproxy_user_exists(host):
    user_exists = False
    target_users = ['haproxy']
    for user_info in pwd.getpwall():
        if user_info.pw_name in target_users:
            user_exists = True
            break
    assert user_exists

def test_docker_group_exists(host):
    group_exists = False
    target_groups = ['haproxy']
    for group_info in grp.getgrall():
        if group_info.gr_name in target_groups:
            group_exists = True
            break
    assert group_exists
    
